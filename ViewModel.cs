﻿using System;
using System.Runtime.InteropServices;
using GalaSoft.MvvmLight;

namespace CG_Lab1_WPF
{
    public class ViewModel : ViewModelBase
    {
        private double _R;
        private double _G;
        private double _B;
        private double _C;
        private double _M;
        private double _Y;
        private double _K;

        public double R
        {
            get => _R;
            set
            {
                if (_R != value)
                {
                    _R = value;
                    UpdateCMYK(_R, _G, _B);
                    RaisePropertyChanged(nameof(R));
                }
            }
        }

        public double G
        {
            get => _G;
            set
            {
                if (_G != value)
                {
                    _G = value;
                    UpdateCMYK(_R, _G, _B);
                    RaisePropertyChanged(nameof(G));
                }
            }
        }

        public double B
        {
            get => _B;
            set
            {
                if (_B != value)
                {
                    _B = value;
                    UpdateCMYK(_R, _G, _B);
                    RaisePropertyChanged(nameof(B));
                }
            }
        }

        public double C
        {
            get => _C;
            set
            {
                if (_C != value)
                {
                    _C = value;
                    UpdateRGB(_C, _M, _Y, _K);
                    RaisePropertyChanged(nameof(C));
                }
            }
        }

        public double M
        {
            get => _M;
            set
            {
                if (_M != value)
                {
                    _M = value;
                    UpdateRGB(_C, _M, _Y, _K);
                    RaisePropertyChanged(nameof(M));
                }
            }
        }

        public double Y
        {
            get => _Y;
            set
            {
                if (_Y != value)
                {
                    _Y = value;
                    UpdateRGB(_C, _M, _Y, _K);
                    RaisePropertyChanged(nameof(Y));
                }
            }
        }

        public double K
        {
            get => _K;
            set
            {
                if (_K != value)
                {
                    _K = value;
                    UpdateRGB(_C, _M, _Y, _K);
                    RaisePropertyChanged(nameof(K));
                }
            }
        }

        public void UpdateRGB(double c, double m, double y, double k)
        {
            _R = 255 * (1 - c) * (1 - k);
            _G = 255 * (1 - m) * (1 - k);
            _B = 255 * (1 - y) * (1 - k);
            RaisePropertyChanged(nameof(R));
            RaisePropertyChanged(nameof(G));
            RaisePropertyChanged(nameof(B));
        }

        public void UpdateCMYK(double r, double g, double b)
        {
            var R_ = r / 255.0;
            var G_ = g / 255.0;
            var B_ = b / 255.0;
            _K = 1 - Math.Max(R_, Math.Max(G_, B_));
            _C = (1 - R_ - K) / (1 - K);
            _M = (1 - G_ - K) / (1 - K);
            _Y = (1 - B_ - K) / (1 - K);
            RaisePropertyChanged(nameof(C));
            RaisePropertyChanged(nameof(M));
            RaisePropertyChanged(nameof(Y));
            RaisePropertyChanged(nameof(K));
        }

        public ViewModel()
        {
            B = 10;
        }
    }
}